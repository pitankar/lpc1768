GCC_BIN = ""
PROJECT = main
OBJECTS = system_LPC17xx.o startup_LPC17xx.o main.o 
SYS_OBJECTS = 
INCLUDE_PATHS = -I. -I./inc
LIBRARY_PATHS = 
LIBRARIES = 
LINKER_SCRIPT = ./inc/LPC1768.ld
BUILD_DIR = build/
RED_LINK = /usr/local/lpcxpresso_8.0.0_526/lpcxpresso/bin/crt_emu_cm_redlink
############################################################################### 
AS      = $(GCC_BIN)arm-none-eabi-as
CC      = $(GCC_BIN)arm-none-eabi-gcc
CPP     = $(GCC_BIN)arm-none-eabi-g++
LD      = $(GCC_BIN)arm-none-eabi-gcc
OBJCOPY = $(GCC_BIN)arm-none-eabi-objcopy

CCLOCAL = gcc

CPU = -mcpu=cortex-m3 -mthumb
CC_FLAGS = $(CPU) -c -fno-common -fmessage-length=0 -Wall -fno-exceptions -ffunction-sections -fdata-sections -g 
CC_SYMBOLS = -DTARGET_LPC1768 -DTOOLCHAIN_GCC_ARM -DNDEBUG -D__CORTEX_M3

LD_FLAGS = -mcpu=cortex-m3 -mthumb -Wl,--gc-sections,-Map=$(PROJECT).map,--cref --specs=nano.specs
LD_SYS_LIBS = -lc -lgcc -lnosys

all: $(PROJECT).bin

clean:
	@echo "Cleaning Project"
	@rm -f $(PROJECT).bin
	@echo "Done!"

.s.o:
	@mkdir -p $(BUILD_DIR)
	@$(AS) $(CPU) -o $(addprefix $(BUILD_DIR), $@) $<

.c.o:
	@mkdir -p $(BUILD_DIR)
	@$(CC)  $(CC_FLAGS) $(CC_SYMBOLS) -std=gnu99   $(INCLUDE_PATHS) -o $(addprefix $(BUILD_DIR), $@) $<

.cpp.o:
	@mkdir -p $(BUILD_DIR)
	@$(CPP) $(CC_FLAGS) $(CC_SYMBOLS) -std=gnu++98 $(INCLUDE_PATHS) -o $(addprefix $(BUILD_DIR), $@) $<

$(PROJECT).elf: $(OBJECTS) $(SYS_OBJECTS)
	@$(LD) $(LD_FLAGS) -T$(LINKER_SCRIPT) $(LIBRARY_PATHS) -o $@ $(addprefix $(BUILD_DIR), $^) $(LIBRARIES) $(LD_SYS_LIBS) $(LIBRARIES) $(LD_SYS_LIBS)

$(PROJECT).bin: $(PROJECT).elf
	@echo "Building the Binary... "
	@$(OBJCOPY) -O binary $(PROJECT).elf $(PROJECT).bin
	@rm -f $(PROJECT).elf $(addprefix $(BUILD_DIR), $(OBJECTS)) $(PROJECT).map
	@rm -rf $(BUILD_DIR)
	@echo "Done!"

flash: $(PROJECT).bin
	@echo "Uploading the binary... "
	@$(RED_LINK) -flash-load-exec $(PROJECT).bin -g -2 -vendor=NXP -pLPC1768 -load-base=0x00000000 -reset=vectreset -flash-driver=LPC175x_6x_512.cfx > /dev/null 2>&1
	@echo "Done!"
erase:
	@echo "Erasing the Memory... "
	@$(RED_LINK) -flash-mass -g -2 -vendor=NXP -pLPC1768 -load-base=0x00000000 -reset=vectreset -flash-driver=LPC175x_6x_512.cfx > /dev/null 2>&1
	@echo "Done!"
